const {op} = require('sequelize');
const Product = require('../models/Product');
const User = require('../models/User');

const index = async(req, res ) =>{
    try{
        const products = await Product.findAll();
        return res.status(200).json({products});
    } catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req, res) =>{
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    } catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req, res) =>{
    const {userId} = req.params;
    try{
        const newProduct = {
            name : req.body.name,
            price : req.body.price,
            description : req.body.description,
            assessments : req.body.assessments,
            datasheet : req.body.datasheet,
            productReviews : req.body.productReviews,
            paymentMethod : req.body.paymentMethod
        };
        const product = await Product.create(newProduct);
        const user = await User.findByPk(userId);
        await product.setUser(user);
        return res.status(201).json({message: 'Produto cadastrado com sucesso.', product: product});
    } catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Product.update(req.body, {where: {id: id}});
        if(updated) {
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json('Produto não encontrado');
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Produto deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
};

const purchase = async(req,res) => {
    const {productId,userId } = req.params;
    try {
        const product = await Product.findByPk(productId);
        const user = await User.findByPk(userId);
        await product.setUser(user);
        return res.status(200).json({msg: "Compra concluída com sucesso."});
    }catch(err){
        return res.status(500).json({err});
    }
};

const cancelPurchase = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        await product.setUser(null);
        return res.status(200).json({msg: "Compra cancelada com sucesso."});
    }catch(err){
        return res.status(500).json({err});
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    purchase,
    cancelPurchase
};
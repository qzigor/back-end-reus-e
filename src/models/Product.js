const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");


const Product = sequelize.define('Product', {
    name: {
        type : DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    productReviews: {
        type: DataTypes.STRING,
        allowNull: false
    },
    datasheet: {
        type: DataTypes.STRING,
        allowNull: false
    },
    paymentMethod: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Product.associate = function(models){
    Product.belongsTo(models.User);
};

module.exports = Product;
const { Router } = require("express");
const router = Router();
const UserControllers = require("../controllers/UserControllers");
const ProductControllers = require("../controllers/ProductControllers");


/* Rotas do usuário */

router.get("/User",UserControllers.index);
router.get("/User/:id", UserControllers.show);
router.put("/User/:id", UserControllers.update);
router.delete("/User/:id", UserControllers.destroy);
router.post("/User", UserControllers.create);


/*Rotas dos produtos */

router.get("/Product", ProductControllers.index);
router.get("/Product/:id", ProductControllers.show);
router.put("/Product/:id", ProductControllers.update);
router.delete("/Product/:id", ProductControllers.destroy);
router.post("/Product/:userId", ProductControllers.create);
router.put("/Product/purchase/:productId/User/:userId",ProductControllers.purchase);
router.put("/Product/cancelPurchase/:id",ProductControllers.cancelPurchase);


module.exports = router;
